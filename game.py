import math
import random
import time


from easygame import *

def isSolid(solid, pos):
    if abs(solid[int(pos[1])//16][int(pos[0])//16][0] - 1.0) < 0.001:
        return False
    return True

def dist(a, b):
    return math.hypot(a[0] - b[0], a[1] - b[1])

open_window('Fifiplesk', 1920,1080)

song = load_audio("music.wav", streaming=False)
gunshot = load_audio("gunshot.wav", streaming=False)
play_audio(audio=song, channel=1, loop=True, volume=1)

human = [load_image("images/human.png"), load_image("images/human_weapon.png")]
zombie = load_image("images/zombie.png")
map = load_image("images/map.png")
mapmask = load_image("images/mapmask.png")
shot = load_image("images/shot.png")
solid = image_data(mapmask)
heal = load_image("images/heal.png")

should_quit = False
up, left = 0, 0
ms = 9
cx, cy = 1920/2, 1080/2
while not should_quit:
    playerPos = (1200, 600)
    playerRot = 0
    playerHp = 200
    zombiePos = []
    zombieRot = []
    zombieHp = []
    heals = [(7350, 3580), (9090, 2190), (1400, 5080)]
    for i in range(10):
        x = random.random() * 300 + 1160
        y = random.random() * 300 + 2160
        zombiePos.append((x, y))
        zombieRot.append(0)
        zombieHp.append(3)
    for i in range(15):
        x = random.random() * 300 + 1360
        y = random.random() * 300 + 5100
        zombiePos.append((x, y))
        zombieRot.append(0)
        zombieHp.append(3)
    for i in range(20):
        x = random.random() * 600 + 5400
        y = random.random() * 600 + 1800
        zombiePos.append((x, y))
        zombieRot.append(0)
        zombieHp.append(3)
    for i in range(25):
        x = random.random() * 800 + 7680
        y = random.random() * 800 + 1800
        zombiePos.append((x, y))
        zombieRot.append(0)
        zombieHp.append(3)
    for i in range(25):
        x = random.random() * 1600 + 5000
        y = random.random() * 1600 + 2000
        zombiePos.append((x, y))
        zombieRot.append(0)
        zombieHp.append(3)
    for i in range(25):
        x = random.random() * 1600 + 6000
        y = random.random() * 1600 + 2000
        zombiePos.append((x, y))
        zombieRot.append(0)
        zombieHp.append(3)
    for i in range(25):
        x = random.random() * 800 + 7500
        y = random.random() * 800 + 4000
        zombiePos.append((x, y))
        zombieRot.append(0)
        zombieHp.append(3)
    for i in range(25):
        x = random.random() * 100 + 9500
        y = random.random() * 100 + 5400
        zombiePos.append((x, y))
        zombieRot.append(0)
        zombieHp.append(3)
    for i in range(100):
        x = random.randrange(960, 10560)
        y = random.randrange(540, 5940)
        zombiePos.append((x, y))
        zombieRot.append(0)
        zombieHp.append(3)

    projectiles = []

    cnt = 0
    kills = 0
    while not should_quit:
        cnt += 1
        shoot = False
        for event in poll_events():
            if type(event) is CloseEvent:
                should_quit = True
            if type(event) is KeyDownEvent:
                if event.key is "UP":
                    up = 1
                if event.key is "DOWN":
                    up = -1
                if event.key is "LEFT":
                    left = 1
                if event.key is "RIGHT":
                    left = -1
                if event.key is "W":
                    up = 1
                if event.key is "S":
                    up = -1
                if event.key is "A":
                    left = 1
                if event.key is "D":
                    left = -1
            if type(event) is KeyUpEvent:
                if event.key is "UP":
                    up = 0
                if event.key is "DOWN":
                    up = 0
                if event.key is "LEFT":
                    left = 0
                if event.key is "RIGHT":
                    left = 0
                if event.key is "W":
                    up = 0
                if event.key is "S":
                    up = 0
                if event.key is "A":
                    left = 0
                if event.key is "D":
                    left = 0
            if type(event) is MouseMoveEvent:
                if abs(cy - event.y) < 5 and abs(cx - event.x) < 5:
                    continue
                y = event.y - cy
                x = event.x - cx
                playerRot = math.atan2(y, x) + math.asin(min(1, max(-1, 24 / dist((event.x, event.y), (cx, cy)))))
            if type(event) is MouseUpEvent:
                if event.button is "LEFT":
                    shoot = True

        if len(zombiePos) < 100:
            for i in range(50):
                x = random.randrange(960, 10560)
                y = random.randrange(540, 5940)
                zombiePos.append((x, y))
                zombieRot.append(0)
                zombieHp.append(3)
        
        ok = True
        if up is 0 or left is 0:
            newPos = (playerPos[0] - ms * left, playerPos[1] + ms * up)
        else:
            newPos = (playerPos[0] - ms * left / math.sqrt(2), playerPos[1] + ms * up / math.sqrt(2))
        for i in range(len(zombiePos)):
            if dist(newPos, zombiePos[i]) <= 24:
                ok = False
                break

        if ok and not isSolid(solid, newPos):
            playerPos = newPos

        i = 0
        while i in range(len(heals)):
            if dist(playerPos, heals[i]) <= 48:
                playerHp = 200
                heals.pop(i)
                i -= 1
            i += 1
        
        if shoot:
            source = (24 * math.sin(playerRot), -24 * math.cos(playerRot))
            projectiles.append([(playerPos[0] + source[0], playerPos[1] + source[1]), playerPos, playerRot])
            play_audio(gunshot, channel=2, loop=False, volume=1)

        for i in range(len(projectiles)):
            newPos = (projectiles[i][0][0] + math.cos(projectiles[i][2]) * 12, projectiles[i][0][1] + math.sin(projectiles[i][2]) * 12)
            projectiles[i][0] = newPos
        
        i = 0
        while i < len(projectiles):
            if dist(projectiles[i][0], projectiles[i][1]) >= 2000:
                projectiles.pop(i)
                i -= 1
            i += 1

        i = 0
        while i < len(zombiePos):
            j = 0
            while i < len(zombiePos) and j < len(projectiles):
                if dist(projectiles[j][0], zombiePos[i]) < 24:
                    projectiles.pop(j)
                    j -= 1
                    zombieHp[i] -= 1
                    if zombieHp[i] is 0:
                        zombieHp.pop(i)
                        zombiePos.pop(i)
                        zombieRot.pop(i)
                        i -= 1
                        kills += 1
                        break
                j += 1
            i += 1



        for i in range(len(zombiePos)):
            if dist(zombiePos[i], playerPos) > 600:
                if (cnt + i * 60 // len(zombiePos)) % 60 == 0:
                    zombieRot[i] = random.random() * 2 * math.pi
                newPos = (zombiePos[i][0] + math.cos(zombieRot[i]), zombiePos[i][1] + math.sin(zombieRot[i]))
                if not isSolid(solid, newPos):
                    zombiePos[i] = newPos
                continue
            theta = math.atan2(playerPos[1] - zombiePos[i][1], playerPos[0] - zombiePos[i][0])
            newPos = (zombiePos[i][0] + math.cos(theta) * 6, zombiePos[i][1] + math.sin(theta) * 6)
            ok = True
            for j in range(len(zombiePos)):
                if j is i:
                    continue
                if dist(newPos, zombiePos[j]) < 20:
                    ok = False
                    break
            if not isSolid(solid, newPos) and ok and dist(playerPos, newPos) > 20:
                zombiePos[i] = newPos
                zombieRot[i] = theta
            if dist(playerPos, zombiePos[i]) <= 48:
                playerHp -= 1
        set_camera(center=(1920/2, 1080/2), position=(playerPos))
        draw_image(map, (1920*3, 1080*3))
        set_camera(center=(1920/2, 1080/2), position=playerPos)
        for i in range(len(projectiles)):
            draw_image(shot, projectiles[i][0], (32, 32), projectiles[i][2])
        for i in range(len(zombiePos)):
            draw_image(zombie, zombiePos[i], (17, 32), zombieRot[i])
        for i in range(len(heals)):
            draw_image(heal, heals[i], (32, 32))
        draw_image(human[1], playerPos, (17, 32), playerRot)
        save_camera()
        reset_camera()
        draw_polygon((1700, 1040), (1900, 1040), (1900, 1060), (1700, 1060), color=(0, 0, 0, 1))
        draw_polygon((1700, 1040), (1700 + playerHp, 1040), (1700 + playerHp, 1060), (1700, 1060), color=(1, 0.8, 0.8, 1))
        draw_text("Kills: " + str(kills), 'Courier New', 20, (1700, 1010), (1, 1, 1, 1))
        draw_text("Zombies: " + str(len(zombiePos)), 'Courier New', 20, (1700, 980), (1, 1, 1, 1))
        if playerHp <= 0:
            draw_text("You died!!!", 'Courier New', 200, (100, 540), (1, 1, 1, 1))
            next_frame()
            restore_camera()
            time.sleep(2)
            break
        if dist(playerPos, (9800, 5280)) < 150:
            draw_text("You win :3", 'Courier New', 220, (100, 540), (1, 1, 1, 1))
            next_frame()
            restore_camera()
            time.sleep(2)
            break
        restore_camera()
        next_frame()

close_window()
